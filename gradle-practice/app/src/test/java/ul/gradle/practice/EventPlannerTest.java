package ul.gradle.practice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import ul.gradle.practice.model.Event;
import ul.gradle.practice.service.EventPlanner;

public class EventPlannerTest {
    @Test
    public void testEventCount() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("/Users/testvagrant/Documents/ultralesson-gradle-practice/gradle-practice/gradle.properties"));
        int eventCount = Integer.parseInt(properties.getProperty("eventCount"));
        EventPlanner eventPlanner = new EventPlanner();

        for (int i = 0; i < eventCount; i++) {
            Event event = new Event("Event " + (i + 1), "2023-06-02", "Online");
            eventPlanner.addEvent(event);
        }

        assertEquals(eventCount, eventPlanner.getEvents().size());
    }

    @Test
    public void testAddEvent() {
        EventPlanner eventPlanner = new EventPlanner();
        Event event = new Event("Course Launch", "2023-06-02", "Online");
        eventPlanner.addEvent(event);
        Assertions.assertEquals(1, eventPlanner.getEvents().size());
        Assertions.assertEquals(event, eventPlanner.getEvents().get(0));
    }
    @Test
  public void testGetEventsByCategory() {
  EventPlanner eventPlanner = new EventPlanner();
  Event onlineEvent = new Event("Gradle Course Launch", "2023-06-02", "Online");
  onlineEvent.setCategory("online");
  Event offlineEvent = new Event("Gradle Workshop", "2023-06-05", "Seattle");
  offlineEvent.setCategory("offline");
  eventPlanner.addEvent(onlineEvent);
  eventPlanner.addEvent(offlineEvent);
  
  List<Event> onlineEvents = eventPlanner.getEventsByCategory("online");
  assertEquals(1, onlineEvents.size());
  assertEquals(onlineEvent, onlineEvents.get(0));
}
}
// /Users/testvagrant/Documents/ul-gradle-practice/gradle/wrapper